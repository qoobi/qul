from setuptools import setup, find_packages


setup(
    name='qul',
    version='0.2',
    description='Qoobi utility library',
    url='https://bitbucket.org/qoobi/qul',
    author='Mikhail Gilmutdinov',
    author_email='l@qoobi.me',
    license='MIT',
    packages=find_packages(),
)
