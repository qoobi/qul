from .misc import extends, aioalt
from .kolektoj import tabelo, vico, vortaro
from . import pklgz

__version__ = '0.2'
