import gzip
import pickle


def dump(obj, filename):
    with gzip.open(filename, 'wb') as f:
        pickle.dump(obj, f, 4)


def load(filename):
    with gzip.open(filename, 'rb') as f:
        return pickle.load(f)
