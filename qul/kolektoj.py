from collections import Iterable, Hashable
from copy import copy, deepcopy

class tabelo:
    def __init__(self, *args):
        if len(args) == 1 and isinstance(args[0], Iterable):
            self.__list__ = list(args[0])
        else:
            self.__list__ = list(args)

    def __getattr__(self, attr):
        return tabelo(getattr(elem, attr) for elem in self)

    def __iter__(self):
        return iter(self.__list__)

    def __reversed__(self):
        return reversed(self.__list__)

    def __repr__(self):
        return 'tabelo[\n ' + ',\n '.join(self.apply(repr)) + '\n]'

    def __len__(self):
        return len(self.__list__)

    def __contains__(self, elem):
        return elem in self.__list__

    def __getitem__(self, key):
        if isinstance(key, int):
            return self.__list__[key]
        if (isinstance(key, Iterable)
            and all(isinstance(k, int) for k in key)):
            return tabelo(self.__list__[k] for k in key)
        raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, int):
            self.__list__[key] = value
        elif (isinstance(key, Iterable)
            and isinstance(value, Iterable)
            and all(isinstance(k, int) for k in key)
            and len(key) == len(value)):
            for k, v in zip(key, value):
                self[k] = v
        else:
            raise TypeError

    def __delitem__(self, key):
        if isinstance(key, int):
            del self.__list__[key]
        elif (isinstance(key, Iterable)
            and all(isinstance(k, int) for k in key)):
            for k in key:
                del self[k]
        else:
            raise TypeError

    def __bool__(self):
        return bool(self.__list__)

    def __getstate__(self):
        return copy(self.__list__)

    def __setstate__(self, state):
        self.__list__ = copy(state)

    def __eq__(self, other):
        return self.__list__ == other.__list__

    def append(self, elem):
        self.__list__.append(copy(elem))

    def extend(self, other):
        self.__list__.extend(copy(other))

    def apply(self, func):
        return tabelo(func(elem) for elem in self)
    each = apply


class vico(tabelo):
    def __repr__(self):
        return 'vico[' + ', '.join(self.apply(repr)) + ' ]'


class vortaro:
    def __init__(self, *args, **kwargs):
        if len(args) == 1 and not kwargs:
            self.__dict__ = dict(args[0])
        elif kwargs and not args:
            self.__dict__ = kwargs
        else:
            raise TypeError

    def __repr__(self):
        return 'vortaro[ ' + ', '.join('='.join((str(k), str(v))) for k, v in self.items()) + ' ]'

    def items(self):
        return self.__dict__.items()

    def __getitem__(self, key):
        if isinstance(key, Hashable) and key in self.__dict__:
            return self.__dict__[key]
        return vortaro({k: self.__dict__[k] for k in key})
