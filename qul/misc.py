import types


def extends(cls):
    def dcr(obj):
        if isinstance(obj, types.FunctionType):
            name = obj.__name__
        elif isinstance(obj, (classmethod, staticmethod)):
            name = obj.__get__(cls).__name__
        elif isinstance(obj, property):
            name = obj.fget.__name__
        else:
            raise Exception
        setattr(cls, name, obj)
    return dcr


def aioalt(func):
    def alt(self, *args, **kwargs):
        if self.aio:
            name = f'aio{func.__name__}'
            try:
                method = getattr(self, name)
            except AttributeError:
                raise Exception
            return method(*args, **kwargs)
        else:
            return func(self, *args, **kwargs)
    return alt
